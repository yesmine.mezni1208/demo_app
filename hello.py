from flask import Flask
import sentry_sdk

app = Flask(__name__)
@app.route("/")
def hello():
    return "*** Welcome to our cloud provider Kubermachines ********** "
if __name__ == "__main__": 
    sentry_sdk.init(
    "https://12108437ec844acf924c0f52fed2ef3c@o342040.ingest.sentry.io/5544535",
    traces_sample_rate=1.0
     )
    app.run(host="0.0.0.0", debug=True)